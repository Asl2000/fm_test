import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_fm/domain/di/locator.dart';
import 'package:test_fm/domain/localization/localization.dart';
import 'package:test_fm/domain/services/settings_service.dart';
import 'package:test_fm/ui/resurses/themes/dark_theme.dart';
import 'package:test_fm/ui/resurses/themes/light_theme.dart';
import 'package:test_fm/ui/route.dart';
import 'package:test_fm/ui/screens/chacters/chacters_screen.dart';
import 'package:test_fm/ui/state_manager/store.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main() {
  final locator = LocatorService();
  runApp(MyApp(locator: locator));
}

class MyApp extends StatelessWidget {
  final LocatorService locator;

  const MyApp({
    super.key,
    required this.locator,
  });

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: locator.store,
      child: ChangeNotifierProvider<SettingsService>(
        create: (BuildContext context) => SettingsService(),
        builder: (context, child) {
          final settingsService = Provider.of<SettingsService>(context);
          return MaterialApp(
            theme: settingsService.isDark ? darkThemeData : lightThemeData,
            navigatorKey: locator.navigatorKey,
            locale: settingsService.language.locale,
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalization.languages.map((e) => e.locale).toList(),
            home: const CharactersScreen(),
            onGenerateRoute: AppRoutes.onGenerateRoute,
          );
        },
      ),
    );
  }
}
