import 'package:test_fm/data/deserializer/deserializer.dart';
import 'package:test_fm/domain/enums/status.dart';

class StatusDeserializer implements Deserializer<Status, String> {
  @override
  call(scheme) {
    final map = {
      'Alive': Status.alive,
      'Dead': Status.dead,
    };
    return map[scheme] ?? Status.unknown;
    switch (scheme) {
      case 'Alive':
        return Status.alive;
      case 'Dead':
        return Status.dead;
      default:
        return Status.unknown;
    }
  }
}
