import 'package:flutter/cupertino.dart';
import 'package:test_fm/data/model/language.dart';
import 'package:test_fm/domain/localization/localization.dart';
import 'package:test_fm/domain/services/shared_preference_service.dart';

class SettingsService extends ChangeNotifier {
  bool _isDark = false;
  Language _language = AppLocalization.languages.first;

  SettingsService() {
    _setThemeFromCash();
    _setLanguageFromCash();
  }

  bool get isDark => _isDark;

  Language get language => _language;

  void _setThemeFromCash() async {
    _isDark = await SharedPreferenceService.getTheme();
    notifyListeners();
  }

  void _setLanguageFromCash() async {
    final locale = await SharedPreferenceService.getLanguage();
    if (locale != null) {
      _language = AppLocalization.languages.firstWhere(
        (e) => e.locale.languageCode == locale.languageCode,
      );
      notifyListeners();
    }
  }

  void switchLanguage({required Language language}) async {
    _language = language;
    SharedPreferenceService.setLanguage(language.locale);
    notifyListeners();
  }

  void switchTheme() async {
    _isDark = !_isDark;
    SharedPreferenceService.setTheme(_isDark);
    notifyListeners();
  }
}
