import 'package:flutter/cupertino.dart';
import 'package:test_fm/data/model/character.dart';
import 'package:test_fm/ui/route.dart';

class NavigatorService {
  final GlobalKey<NavigatorState> navigatorKey;

  NavigatorService({required this.navigatorKey});

  void onSettings() {
    navigatorKey.currentState!.pushNamed(AppRoutes.setting);
  }

  void onCharacterDetail({required CharacterModel character}) {
    navigatorKey.currentState!.pushNamed(
      AppRoutes.characterDetail,
      arguments: AppRouterArguments(character: character),
    );
  }
}
