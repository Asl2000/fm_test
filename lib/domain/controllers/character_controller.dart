import 'package:test_fm/domain/repositories/character_repository.dart';

class CharacterController {
  final CharacterRepository _characterRepository;

  CharacterController({required CharacterRepository characterRepository})
      : _characterRepository = characterRepository;

  void addFavorite({required int id}) async {
    //Message
    await _characterRepository.addFavoriteCharacter(id: id);
    //Ui
  }

  void removeFavorite({required int id}) async {
    await _characterRepository.removeFavoriteCharacter(id: id);
  }
}
