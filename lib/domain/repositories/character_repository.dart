import 'package:test_fm/data/model/answer.dart';
import 'package:test_fm/data/model/character.dart';

abstract class CharacterRepository {
  Future<Answer<List<CharacterModel>>> getListCharacter();

  Future<bool> addFavoriteCharacter({required int id});

  Future<bool> removeFavoriteCharacter({required int id});
}
