import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:redux/redux.dart';
import 'package:test_fm/data/api/api.dart';
import 'package:test_fm/data/repositories/api_character_repository.dart';
import 'package:test_fm/domain/controllers/character_controller.dart';
import 'package:test_fm/domain/repositories/character_repository.dart';
import 'package:test_fm/domain/services/navigator_service.dart';
import 'package:test_fm/ui/state_manager/characters/middlewares.dart';
import 'package:test_fm/ui/state_manager/characters/states.dart';
import 'package:test_fm/ui/state_manager/profile/states.dart';
import 'package:test_fm/ui/state_manager/reduser.dart';
import 'package:test_fm/ui/state_manager/store.dart';

class LocatorService {
  final api = Api();
  final navigatorKey = GlobalKey<NavigatorState>();

  late CharacterRepository characterRepository;
  late NavigatorService navigatorService;

  late Store<AppState> store;
  late CharacterController characterController;

  LocatorService() {
    characterRepository = ApiCharacterRepository(api: api);
    navigatorService = NavigatorService(navigatorKey: navigatorKey);

    characterController = CharacterController(characterRepository: characterRepository);

    store = Store(
      appReducer,
      initialState: AppState(
        profileState: ProfileState(),
        characterListState: CharacterListState(),
      ),
      middleware: [
        CharacterMiddleware(characterRepository: characterRepository).call,
      ],
    );

    _register();
  }

  void _register() {
    GetIt.I.registerSingleton<NavigatorService>(navigatorService);
    GetIt.I.registerSingleton<CharacterController>(characterController);
  }
}
