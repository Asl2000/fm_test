import 'package:get_it/get_it.dart';
import 'package:test_fm/domain/controllers/character_controller.dart';
import 'package:test_fm/domain/services/navigator_service.dart';

class GetItServices {
  NavigatorService get navigatorService => GetIt.I.get<NavigatorService>();

  CharacterController get characterController => GetIt.I.get<CharacterController>();
}

final getItService = GetItServices();
