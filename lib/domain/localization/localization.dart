import 'package:flutter/cupertino.dart';
import 'package:test_fm/data/model/language.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

extension LocalizationContext on BuildContext {
  AppLocalizations get loc => AppLocalizations.of(this)!;
}

class AppLocalization {
  static List<Language> languages = [
    Language(
      name: 'Русский',
      locale: const Locale('ru'),
    ),
    Language(
      name: 'English',
      locale: const Locale('en'),
    ),
  ];
}
