import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:test_fm/data/model/language.dart';
import 'package:test_fm/domain/localization/localization.dart';
import 'package:test_fm/domain/services/settings_service.dart';
import 'package:test_fm/ui/resurses/colors.dart';
import 'package:test_fm/ui/resurses/text.dart';

class LanguageModal extends StatefulWidget {
  const LanguageModal({super.key});

  @override
  State<LanguageModal> createState() => _LanguageModalState();
}

class _LanguageModalState extends State<LanguageModal> {
  late Language currentLanguage;
  late SettingsService settingsService;

  @override
  void initState() {
    super.initState();
    settingsService = Provider.of<SettingsService>(context, listen: false);
    currentLanguage = settingsService.language;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    currentLanguage = Provider.of<SettingsService>(context).language;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 25,
        vertical: 16,
      ),
      width: double.infinity,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 4,
            width: 38,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: Colors.black,
            ),
          ),
          const SizedBox(height: 10),
          Text(
            context.loc.chooseLanguage,
            style: GoogleFonts.montserrat(
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(height: 16),
          for (var language in AppLocalization.languages) ...[
            GestureDetector(
              onTap: () {
                settingsService.switchLanguage(language: language);
                Navigator.of(context).pop();
              },
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 5,
                ),
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(12),
                  border: currentLanguage.locale.languageCode == language.locale.languageCode
                      ? Border.all(color: AppColors.primary)
                      : null,
                ),
                child: Text(
                  language.name,
                  style: AppText.text1,
                ),
              ),
            ),
            const SizedBox(height: 8),
          ],
        ],
      ),
    );
  }
}
