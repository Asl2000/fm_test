import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:test_fm/data/model/language.dart';
import 'package:test_fm/domain/localization/localization.dart';
import 'package:test_fm/domain/services/settings_service.dart';
import 'package:test_fm/ui/resurses/colors.dart';
import 'package:test_fm/ui/resurses/icons.dart';
import 'package:test_fm/ui/screens/settings/widgets/language_modal.dart';
import 'package:test_fm/ui/widgets/app_card.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  late bool isDark;
  late Language language;
  late SettingsService settingsService;

  @override
  void initState() {
    super.initState();
    settingsService = Provider.of<SettingsService>(context, listen: false);
    isDark = settingsService.isDark;
    language = settingsService.language;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final provider = Provider.of<SettingsService>(context);
    isDark = provider.isDark;
    language = provider.language;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: AppColors.primary,
          ),
          onPressed: Navigator.of(context).pop,
        ),
        title: Text(context.loc.settings),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        children: [
          AppCard(
            child: Row(
              children: [
                SvgPicture.asset(
                  isDark ? AppIcons.moon : AppIcons.sun,
                  width: 24,
                  height: 24,
                  colorFilter: ColorFilter.mode(
                    Theme.of(context).appBarTheme.iconTheme!.color!,
                    BlendMode.srcIn,
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Text(
                    context.loc.theme,
                    style: GoogleFonts.montserrat(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                CupertinoSwitch(
                  activeColor: AppColors.primary,
                  value: isDark,
                  onChanged: (val) {
                    settingsService.switchTheme();
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 8),
          AppCard(
            onTap: () {
              showModalBottomSheet(
                context: context,
                builder: (_) => const LanguageModal(),
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  context.loc.language,
                  style: GoogleFonts.montserrat(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  language.name,
                  style: GoogleFonts.montserrat(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).textTheme.bodyMedium!.color!.withOpacity(0.5),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
