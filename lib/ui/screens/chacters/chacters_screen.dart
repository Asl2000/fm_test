import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:redux/redux.dart';
import 'package:test_fm/domain/di/get_it_services.dart';
import 'package:test_fm/domain/localization/localization.dart';
import 'package:test_fm/ui/resurses/icons.dart';
import 'package:test_fm/ui/screens/chacters/widgets/characters_card.dart';
import 'package:test_fm/ui/state_manager/characters/actions.dart';
import 'package:test_fm/ui/state_manager/characters/states.dart';
import 'package:test_fm/ui/state_manager/store.dart';

class CharactersScreen extends StatefulWidget {
  const CharactersScreen({super.key});

  @override
  State<CharactersScreen> createState() => _CharactersScreenState();
}

class _CharactersScreenState extends State<CharactersScreen> {
  @override
  void initState() {
    super.initState();
    final store = StoreProvider.of<AppState>(context, listen: false);
    if (store.state.characterListState.characters.isEmpty) {
      store.dispatch(LoadCharacterListAction());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(context.loc.characters),
        actions: [
          IconButton(
            onPressed: getItService.navigatorService.onSettings,
            icon: SvgPicture.asset(
              AppIcons.setting,
              width: 24,
              height: 24,
              colorFilter: ColorFilter.mode(
                Theme.of(context).appBarTheme.iconTheme!.color!,
                BlendMode.srcIn,
              ),
            ),
          ),
        ],
      ),
      body: StoreConnector<AppState, CharacterListState>(
        converter: (Store<AppState> store) => store.state.characterListState,
        builder: (BuildContext context, CharacterListState state) {
          if (state.isLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state.isError) {
            return Center(
              child: Text(state.error),
            );
          }
          if (state.characters.isEmpty) {
            return Center(
              child: Text(context.loc.characterEmpty),
            );
          }
          return ListView.separated(
            itemCount: state.characters.length,
            padding: const EdgeInsets.symmetric(horizontal: 16),
            itemBuilder: (_, index) => CharactersCard(
              character: state.characters[index],
            ),
            separatorBuilder: (_, index) => const SizedBox(height: 8),
          );
        },
      ),
    );
  }
}
