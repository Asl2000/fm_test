import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test_fm/data/model/character.dart';
import 'package:test_fm/domain/di/get_it_services.dart';
import 'package:test_fm/ui/resurses/icons.dart';
import 'package:test_fm/ui/resurses/text.dart';
import 'package:test_fm/ui/widgets/app_card.dart';

class CharactersCard extends StatelessWidget {
  final CharacterModel character;

  const CharactersCard({
    super.key,
    required this.character,
  });

  @override
  Widget build(BuildContext context) {
    return AppCard(
      onTap: () => getItService.navigatorService.onCharacterDetail(character: character),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(7),
              child: Image.network(
                character.image,
                width: 60,
                height: 52,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Expanded(
            child: Text(
              character.name,
              style: AppText.body,
            ),
          ),
          const SizedBox(width: 22),
          GestureDetector(
            onTap: () {
              //check
              getItService.characterController.addFavorite(id: character.id);
            },
            child: SvgPicture.asset(AppIcons.favorite),
          ),
        ],
      ),
    );
  }
}
