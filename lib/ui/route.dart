import 'package:flutter/material.dart';
import 'package:test_fm/data/model/character.dart';
import 'package:test_fm/ui/screens/chacters/chacter_detail_screen.dart';
import 'package:test_fm/ui/screens/settings/settings_screen.dart';

class AppRoutes {
  static const setting = '/settings';
  static const characterDetail = '/character-detail';

  static MaterialPageRoute onGenerateRoute(RouteSettings settings) {
    final arg = settings.arguments as AppRouterArguments?;

    final routes = <String, WidgetBuilder>{
      AppRoutes.setting: (BuildContext context) => const SettingsScreen(),
      AppRoutes.characterDetail: (BuildContext context) => CharacterDetailScreen(
            character: arg!.character!,
          ),
    };

    WidgetBuilder? builder = routes[settings.name];
    return MaterialPageRoute(builder: (ctx) => builder!(ctx));
  }
}

class AppRouterArguments {
  final CharacterModel? character;
  final String? text;
  final int? number;

  AppRouterArguments({
    this.character,
    this.number,
    this.text,
  });
}
