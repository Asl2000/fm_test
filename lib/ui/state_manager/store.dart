import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_fm/ui/state_manager/characters/states.dart';
import 'package:test_fm/ui/state_manager/profile/states.dart';

part 'store.freezed.dart';

@freezed
class AppState with _$AppState {
  const factory AppState({
    required ProfileState profileState,
    required CharacterListState characterListState,
  }) = _AppState;
}
