import 'package:redux/redux.dart';
import 'package:test_fm/ui/state_manager/characters/actions.dart';
import 'package:test_fm/ui/state_manager/characters/states.dart';

final characterListReducer = combineReducers<CharacterListState>([
  TypedReducer<CharacterListState, LoadCharacterListAction>(_loadCharacterList).call,
  TypedReducer<CharacterListState, ShowCharacterListAction>(_showCharacterList).call,
  TypedReducer<CharacterListState, ErrorCharacterListAction>(_errorCharacterList).call,
]);

CharacterListState _loadCharacterList(
  CharacterListState state,
  LoadCharacterListAction action,
) =>
    state.copyWith(
      isLoading: true,
      isError: false,
    );

CharacterListState _showCharacterList(
  CharacterListState state,
  ShowCharacterListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: false,
      characters: action.characters,
    );

CharacterListState _errorCharacterList(
  CharacterListState state,
  ErrorCharacterListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: true,
      error: action.error,
    );
