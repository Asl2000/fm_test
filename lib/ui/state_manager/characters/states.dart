import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_fm/data/model/character.dart';

part 'states.freezed.dart';

@freezed
class CharacterListState with _$CharacterListState {
  factory CharacterListState([
    @Default(false) bool isLoading,
    @Default(false) bool isError,
    @Default([]) List<CharacterModel> characters,
    @Default('') String error,
  ]) = _CharacterListState;
}
