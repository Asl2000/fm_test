// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'states.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CharacterListState {
  bool get isLoading => throw _privateConstructorUsedError;
  bool get isError => throw _privateConstructorUsedError;
  List<CharacterModel> get characters => throw _privateConstructorUsedError;
  String get error => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CharacterListStateCopyWith<CharacterListState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CharacterListStateCopyWith<$Res> {
  factory $CharacterListStateCopyWith(
          CharacterListState value, $Res Function(CharacterListState) then) =
      _$CharacterListStateCopyWithImpl<$Res, CharacterListState>;
  @useResult
  $Res call(
      {bool isLoading,
      bool isError,
      List<CharacterModel> characters,
      String error});
}

/// @nodoc
class _$CharacterListStateCopyWithImpl<$Res, $Val extends CharacterListState>
    implements $CharacterListStateCopyWith<$Res> {
  _$CharacterListStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isError = null,
    Object? characters = null,
    Object? error = null,
  }) {
    return _then(_value.copyWith(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isError: null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
      characters: null == characters
          ? _value.characters
          : characters // ignore: cast_nullable_to_non_nullable
              as List<CharacterModel>,
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CharacterListStateImplCopyWith<$Res>
    implements $CharacterListStateCopyWith<$Res> {
  factory _$$CharacterListStateImplCopyWith(_$CharacterListStateImpl value,
          $Res Function(_$CharacterListStateImpl) then) =
      __$$CharacterListStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool isLoading,
      bool isError,
      List<CharacterModel> characters,
      String error});
}

/// @nodoc
class __$$CharacterListStateImplCopyWithImpl<$Res>
    extends _$CharacterListStateCopyWithImpl<$Res, _$CharacterListStateImpl>
    implements _$$CharacterListStateImplCopyWith<$Res> {
  __$$CharacterListStateImplCopyWithImpl(_$CharacterListStateImpl _value,
      $Res Function(_$CharacterListStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? isError = null,
    Object? characters = null,
    Object? error = null,
  }) {
    return _then(_$CharacterListStateImpl(
      null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      null == isError
          ? _value.isError
          : isError // ignore: cast_nullable_to_non_nullable
              as bool,
      null == characters
          ? _value._characters
          : characters // ignore: cast_nullable_to_non_nullable
              as List<CharacterModel>,
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CharacterListStateImpl implements _CharacterListState {
  _$CharacterListStateImpl(
      [this.isLoading = false,
      this.isError = false,
      final List<CharacterModel> characters = const [],
      this.error = ''])
      : _characters = characters;

  @override
  @JsonKey()
  final bool isLoading;
  @override
  @JsonKey()
  final bool isError;
  final List<CharacterModel> _characters;
  @override
  @JsonKey()
  List<CharacterModel> get characters {
    if (_characters is EqualUnmodifiableListView) return _characters;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_characters);
  }

  @override
  @JsonKey()
  final String error;

  @override
  String toString() {
    return 'CharacterListState(isLoading: $isLoading, isError: $isError, characters: $characters, error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CharacterListStateImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.isError, isError) || other.isError == isError) &&
            const DeepCollectionEquality()
                .equals(other._characters, _characters) &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isLoading, isError,
      const DeepCollectionEquality().hash(_characters), error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CharacterListStateImplCopyWith<_$CharacterListStateImpl> get copyWith =>
      __$$CharacterListStateImplCopyWithImpl<_$CharacterListStateImpl>(
          this, _$identity);
}

abstract class _CharacterListState implements CharacterListState {
  factory _CharacterListState(
      [final bool isLoading,
      final bool isError,
      final List<CharacterModel> characters,
      final String error]) = _$CharacterListStateImpl;

  @override
  bool get isLoading;
  @override
  bool get isError;
  @override
  List<CharacterModel> get characters;
  @override
  String get error;
  @override
  @JsonKey(ignore: true)
  _$$CharacterListStateImplCopyWith<_$CharacterListStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
