import 'package:redux/redux.dart';
import 'package:test_fm/domain/repositories/character_repository.dart';
import 'package:test_fm/ui/state_manager/characters/actions.dart';
import 'package:test_fm/ui/state_manager/store.dart';

class CharacterMiddleware implements MiddlewareClass<AppState> {
  final CharacterRepository characterRepository;

  CharacterMiddleware({required this.characterRepository});

  @override
  call(Store<AppState> store, action, NextDispatcher next) {
    if (action is LoadCharacterListAction) {
      Future(() async {
        final answer = await characterRepository.getListCharacter();
        if (answer.data != null) {
          store.dispatch(ShowCharacterListAction(characters: answer.data!));
        } else {
          store.dispatch(ErrorCharacterListAction(error: answer.error!));
        }
      });
    }

    next(action);
  }
}
