import 'package:test_fm/data/model/character.dart';

abstract class CharacterListAction {}

class LoadCharacterListAction extends CharacterListAction {}

class ShowCharacterListAction extends CharacterListAction {
  final List<CharacterModel> characters;

  ShowCharacterListAction({required this.characters});
}

class ErrorCharacterListAction extends CharacterListAction {
  final String error;

  ErrorCharacterListAction({required this.error});
}
