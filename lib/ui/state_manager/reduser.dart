import 'package:test_fm/ui/state_manager/characters/actions.dart';
import 'package:test_fm/ui/state_manager/characters/reducers.dart';
import 'package:test_fm/ui/state_manager/profile/actions.dart';
import 'package:test_fm/ui/state_manager/profile/reducers.dart';
import 'package:test_fm/ui/state_manager/store.dart';

AppState appReducer(AppState state, dynamic action) {

  if (action is ProfileAction) {
    final nextState = profileReducer(state.profileState, action);
    return state.copyWith(profileState: nextState);
  } else if (action is CharacterListAction) {
    final nextState = characterListReducer(state.characterListState, action);
    return state.copyWith(characterListState: nextState);
  }

  return state;
}
