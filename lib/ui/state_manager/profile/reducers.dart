import 'package:redux/redux.dart';
import 'package:test_fm/ui/state_manager/profile/actions.dart';
import 'package:test_fm/ui/state_manager/profile/states.dart';

final profileReducer = combineReducers<ProfileState>([
  TypedReducer<ProfileState, LoadProfileAction>(_loadProfile).call,
  TypedReducer<ProfileState, ShowProfileAction>(_showProfile).call,
  TypedReducer<ProfileState, ErrorProfileAction>(_errorProfile).call,
]);

ProfileState _loadProfile(
  ProfileState state,
  LoadProfileAction action,
) =>
    state.copyWith(
      isLoading: true,
      isError: false,
    );

ProfileState _showProfile(
  ProfileState state,
  ShowProfileAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: false,
      name: action.name,
    );

ProfileState _errorProfile(
  ProfileState state,
  ErrorProfileAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: true,
      error: action.error,
    );
