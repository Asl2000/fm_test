import 'package:freezed_annotation/freezed_annotation.dart';

part 'states.freezed.dart';

@freezed
class ProfileState with _$ProfileState {
  factory ProfileState([
    @Default(false) bool isLoading,
    @Default(false) bool isError,
    @Default('') String name,
    @Default('') String error,
  ]) = _ProfileState;
}
