abstract class ProfileAction {}

class LoadProfileAction extends ProfileAction {}

class ShowProfileAction extends ProfileAction {
  final String name;

  ShowProfileAction({required this.name});
}

class ErrorProfileAction extends ProfileAction {
  final String error;

  ErrorProfileAction({required this.error});
}
