class AppIcons {
  static const setting = 'assets/icon/setting.svg';
  static const favoriteSolid = 'assets/icon/favorite_solid.svg';
  static const favorite = 'assets/icon/favorite.svg';
  static const moon = 'assets/icon/moon.svg';
  static const sun = 'assets/icon/sun.svg';
}
