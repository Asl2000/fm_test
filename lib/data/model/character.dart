import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_fm/domain/deserializer/status_deserializer.dart';
import 'package:test_fm/domain/enums/status.dart';

part 'character.g.dart';

@JsonSerializable()
class CharacterModel {
  final int id;
  final String name;
  @JsonKey(name: 'status')
  final String statusName;
  final String species;
  final String gender;
  final String image;

  final Status? statusTest;

  CharacterModel({
    required this.id,
    required this.name,
    required this.statusName,
    required this.species,
    required this.gender,
    required this.image,
    this.statusTest,
  });

  factory CharacterModel.fromJson(Map<String, dynamic> json) => _$CharacterModelFromJson(json);

  Map<String, dynamic> toJson() => _$CharacterModelToJson(this);

  Status get status => StatusDeserializer().call(statusName);
}
