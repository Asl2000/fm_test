// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CharacterModel _$CharacterModelFromJson(Map<String, dynamic> json) =>
    CharacterModel(
      id: (json['id'] as num).toInt(),
      name: json['name'] as String,
      statusName: json['status'] as String,
      species: json['species'] as String,
      gender: json['gender'] as String,
      image: json['image'] as String,
      statusTest: $enumDecodeNullable(_$StatusEnumMap, json['statusTest']),
    );

Map<String, dynamic> _$CharacterModelToJson(CharacterModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'status': instance.statusName,
      'species': instance.species,
      'gender': instance.gender,
      'image': instance.image,
      'statusTest': _$StatusEnumMap[instance.statusTest],
    };

const _$StatusEnumMap = {
  Status.alive: 'alive',
  Status.dead: 'dead',
  Status.unknown: 'unknown',
};
