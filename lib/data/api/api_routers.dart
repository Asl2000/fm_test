class ApiRouters {
  static const character = '/character';
  static const location = '/location';
  static const episode = '/episode';
}
