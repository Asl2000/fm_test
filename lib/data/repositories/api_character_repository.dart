import 'package:dio/dio.dart';
import 'package:test_fm/data/api/api.dart';
import 'package:test_fm/data/api/api_routers.dart';
import 'package:test_fm/data/model/answer.dart';
import 'package:test_fm/data/model/character.dart';
import 'package:test_fm/domain/repositories/character_repository.dart';

class ApiCharacterRepository implements CharacterRepository {
  final Api _client;

  ApiCharacterRepository({required Api api}) : _client = api;

  @override
  Future<Answer<List<CharacterModel>>> getListCharacter() async {
    try {
      final response = await _client.api.get(
        ApiRouters.character,
      );
      final list = response.data['results'] as List;
      final data = list.map((e) => CharacterModel.fromJson(e)).toList();
      return Answer(data: data);
    } on DioException catch (e) {
      return Answer(error: e.message);
    }
  }

  @override
  Future<bool> addFavoriteCharacter({required int id}) {
    return Future.delayed(
      const Duration(seconds: 1),
      () => true,
    );
  }

  @override
  Future<bool> removeFavoriteCharacter({required int id}) {
    return Future.delayed(
      const Duration(seconds: 1),
      () => true,
    );
  }
}
